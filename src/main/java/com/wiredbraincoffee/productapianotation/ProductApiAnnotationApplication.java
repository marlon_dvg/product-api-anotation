package com.wiredbraincoffee.productapianotation;

import com.wiredbraincoffee.productapianotation.model.Product;
import com.wiredbraincoffee.productapianotation.repository.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Flux;

@SpringBootApplication
public class ProductApiAnnotationApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductApiAnnotationApplication.class, args);
    }

    @Bean
    CommandLineRunner init(ProductRepository repository) {
        return args -> {
            Flux<Product> productFlux = Flux.just(
                    new Product(null, "Big Latte", 2.99),
                    new Product(null, "Big Decaf", 2.69),
                    new Product(null, "Green Tea", 3.99),
                    new Product(null, "Espresso", 1.29))
                    .flatMap(repository::save);
            //.flatMap(product -> repository.save(product));

            productFlux
                    .thenMany(repository.findAll())
                    .subscribe(System.out::println);

        };
    }
}
