package com.wiredbraincoffee.productapianotation.repository;

import com.wiredbraincoffee.productapianotation.model.Product;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ProductRepository extends ReactiveMongoRepository<Product, String> {

}
